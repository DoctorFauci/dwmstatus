/*
 * Copy me if you can.
 * by 20h
 */

#define _BSD_SOURCE
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <strings.h>
#include <sys/time.h>
#include <time.h>
#include <sys/types.h>
#include <sys/wait.h>

#include <X11/Xlib.h>

char *tz = "Europe/Rome";

static Display *dpy;

char *
smprintf(char *fmt, ...)
{
	va_list fmtargs;
	char *ret;
	int len;

	va_start(fmtargs, fmt);
	len = vsnprintf(NULL, 0, fmt, fmtargs);
	va_end(fmtargs);

	ret = malloc(++len);
	if (ret == NULL) {
		perror("malloc");
		exit(1);
	}

	va_start(fmtargs, fmt);
	vsnprintf(ret, len, fmt, fmtargs);
	va_end(fmtargs);

	return ret;
}

void
settz(char *tzname)
{
	setenv("TZ", tzname, 1);
}

char *
mktimes(char *fmt, char *tzname)
{
	char buf[129];
	time_t tim;
	struct tm *timtm;

	settz(tzname);
	tim = time(NULL);
	timtm = localtime(&tim);
	if (timtm == NULL)
		return smprintf("");

	if (!strftime(buf, sizeof(buf)-1, fmt, timtm)) {
		fprintf(stderr, "strftime == 0\n");
		return smprintf("");
	}

	return smprintf("%s", buf);
}

void
setstatus(char *str)
{
	XStoreName(dpy, DefaultRootWindow(dpy), str);
	XSync(dpy, False);
}

char *
getcpuutil(void) {
    long double a[4], b[4], loadavg;
    FILE *fp;
    fp = fopen("/proc/stat", "r");
    fscanf(fp, "%*s %Lf %Lf %Lf %Lf", &a[0], &a[1], &a[2], &a[3]);
    fclose(fp);
    sleep(1);
    fp = fopen("/proc/stat", "r");
    fscanf(fp, "%*s %Lf %Lf %Lf %Lf", &b[0], &b[1], &b[2], &b[3]);
    fclose(fp);
    loadavg = ((b[0] + b[1] + b[2]) - (a[0] + a[1] + a[2])) / ((b[0] + b[1] + b[2] + b[3]) - (a[0] + a[1] + a[2] + a[3])) * 100;
    return smprintf(" %.0Lf", loadavg);
}

char *
getmemusage(void) {
    FILE *fp;
    fp = fopen("/proc/meminfo", "r");
    char label[128];
    char unit[10];
    int value;

    int total = 0;
    int used = 0;

    int done = 0;
    while (done != 15) {
        fscanf(fp, "%s %d %s", label, &value, unit);

        if (strncmp(label, "MemTotal:", 128) == 0) {
            total = value;
            used += value;
            done |= 1;
        } else if (strncmp(label, "MemFree:", 128) == 0) {
            used -= value;
            done |= 2;
        } else if (strncmp(label, "Buffers:", 128) == 0) {
            used -= value;
            done |= 4;
        } else if (strncmp(label, "Cached:", 128) == 0) {
            used -= value;
            done |= 8;
        }
    }
    fclose(fp);
    return smprintf(" %d", used * 100 / total);
}

char *
readfile(char *base, char *file)
{
	char *path, line[513];
	FILE *fd;

	memset(line, 0, sizeof(line));

	path = smprintf("%s/%s", base, file);
	fd = fopen(path, "r");
	free(path);
	if (fd == NULL)
		return NULL;

	if (fgets(line, sizeof(line)-1, fd) == NULL) {
		fclose(fd);
		return NULL;
	}
	fclose(fd);

	return smprintf("%s", line);
}

char *
getbattery(char *base)
{
	char *co, status, *bat;
	int descap, remcap, cap;

	descap = -1;
	remcap = -1;

	co = readfile(base, "present");
	if (co == NULL)
		return smprintf("");
	if (co[0] != '1') {
		free(co);
		return smprintf("not present");
	}
	free(co);

	co = readfile(base, "charge_full_design");
	if (co == NULL) {
		co = readfile(base, "energy_full_design");
		if (co == NULL)
			return smprintf("");
	}
	sscanf(co, "%d", &descap);
	free(co);

	co = readfile(base, "charge_now");
	if (co == NULL) {
		co = readfile(base, "energy_now");
		if (co == NULL)
			return smprintf("");
	}
	sscanf(co, "%d", &remcap);
	free(co);

	co = readfile(base, "status");
	if (!strncmp(co, "Discharging", 11)) {
		status = '-';
	} else if(!strncmp(co, "Charging", 8)) {
		status = '+';
	} else {
		status = '?';
	}
	free(co);

	if (remcap < 0 || descap < 0)
		return smprintf("invalid");

	cap = ((float)remcap / (float)descap) * 100;
	if (cap >= 80) {
		bat = smprintf("  %d%%%c", cap, status);
	} else if (cap >= 60 && cap < 80) {
		bat = smprintf("  %d%%%c", cap, status);
	} else if (cap >= 40 && cap < 60) {
		bat = smprintf("  %d%%%c", cap, status);
	} else if (cap >= 20 && cap < 40) {
		bat = smprintf("  %d%%%c", cap, status);
	} else {
		bat = smprintf("  %d%%%c", cap, status);
	}

	return bat;
}

char *
gettemperature(char *base, char *sensor)
{
	char *co, *temp;
	double t;

	co = readfile(base, sensor);
	if (co == NULL)
		return smprintf("");

	t = atof(co) / 1000;
	if (t >= 55) {
		temp = smprintf("%02.0f°C", t);
	} else if (t >= 30 && t < 55) {
		temp = smprintf("%02.0f°C", t);
	} else {
		temp = smprintf("%02.0f°C", t);
	}
	free(co);

	return temp;
}

char *
getcalendar(char *tz)
{
	char *str;
	time_t now = time(NULL);
	struct tm *tm_struct = localtime(&now);

	int hour = tm_struct->tm_hour % 12;

	switch (hour) {
		case 11:
			str = mktimes("[󱑕 %H:%M] [ %d/%m/%Y]", tz);
			break;
		case 10:
			str = mktimes("[󱑔 %H:%M] [ %d/%m/%Y]", tz);
			break;
		case  9:
			str = mktimes("[󱑓 %H:%M] [ %d/%m/%Y]", tz);
			break;
		case  8:
			str = mktimes("[󱑒 %H:%M] [ %d/%m/%Y]", tz);
			break;
		case  7:
			str = mktimes("[󱑑 %H:%M] [ %d/%m/%Y]", tz);
			break;
		case  6:
			str = mktimes("[󱑐 %H:%M] [ %d/%m/%Y]", tz);
			break;
		case  5:
			str = mktimes("[󱑏 %H:%M] [ %d/%m/%Y]", tz);
			break;
		case  4:
			str = mktimes("[󱑎 %H:%M] [ %d/%m/%Y]", tz);
			break;
		case  3:
			str = mktimes("[󱑍 %H:%M] [ %d/%m/%Y]", tz);
			break;
		case  2:
			str = mktimes("[󱑌 %H:%M] [ %d/%m/%Y]", tz);
			break;
		case  1:
			str = mktimes("[󱑋 %H:%M] [ %d/%m/%Y]", tz);
			break;
		case  0:
			str = mktimes("[󱑖 %H:%M] [ %d/%m/%Y]", tz);
			break;
	}

	return str;
}

char *
execscript(char *cmd)
{
	FILE *fp;
	char retval[1025], *rv;

	memset(retval, 0, sizeof(retval));

	fp = popen(cmd, "r");
	if (fp == NULL)
		return smprintf("");

	rv = fgets(retval, sizeof(retval), fp);
	pclose(fp);
	if (rv == NULL)
		return smprintf("");
	retval[strlen(retval)-1] = '\0';

	return smprintf("%s", retval);
}

int
main(void)
{
    char *status;
    char *avgs;
    char *bat;
    char *nbat;
    char *tm;
    char *t0;
    char *memusage;
    FILE *file;

    if (!(dpy = XOpenDisplay(NULL))) {
        fprintf(stderr, "dwmstatus: cannot open display.\n");
        return 1;
    }

    if ((file = fopen("/sys/class/power_supply/BAT0", "r")) != NULL) {
        fclose(file);
        nbat = "/sys/class/power_supply/BAT0";
    } else {
        nbat = "/sys/class/power_supply/BAT1";
    }

    for (;;sleep(30)) {
        avgs = getcpuutil();
        bat = getbattery(nbat);
        tm = getcalendar(tz);
        t0 = gettemperature("/sys/devices/virtual/thermal/thermal_zone0", "temp");
        memusage = getmemusage();

        status = smprintf("[%s] [%s%%] [%s%%] [%s] %s", t0, avgs, memusage, bat, tm);
        setstatus(status);

        free(t0);
        free(avgs);
        free(memusage);
        free(bat);
        free(tm);
        free(status);
    }

    free(nbat);
    XCloseDisplay(dpy);

    return 0;
}

