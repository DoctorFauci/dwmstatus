{
  description = "A very basic flake";

  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs?ref=nixos-unstable";
    flake-utils.url = "github:numtide/flake-utils";
  };

  outputs = { self, nixpkgs, flake-utils, ... }: flake-utils.lib.eachDefaultSystem (system:
    let
      pkgs = nixpkgs.legacyPackages.${system};
      packageName = "dwmstatus";
    in {
      packages.${packageName} = pkgs.stdenv.mkDerivation {
        name = "${packageName}";
	src = self;
	buildInputs = with pkgs; [
          xorg.libX11
        ];
	buildPhase = ''
          gcc -g -std=c99 -pedantic -Wall -O0 -lX11 dwmstatus.c -o dwmstatus
        '';
	installPhase = ''
          mkdir -p $out/bin
          cp dwmstatus $out/bin
        '';
      };
      defaultPackage = self.packages.${system}.${packageName};
    }
  );
}
